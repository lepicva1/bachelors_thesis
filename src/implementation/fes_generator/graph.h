//
// Created by Václav Lepič on 29.03.2022.
//

#ifndef DEM_VC_GRAPH_H
#define DEM_VC_GRAPH_H

#include <vector>
#include <map>
#include <set>
#include <numeric>
#include <iostream>
#include <sstream>
#include <chrono>

class vertex {
public:
    long long int id;
    //set to false if can't belong to edge monitoring set
    bool could_belong_monitoring_set = true;
    //set is chosen to make removal of unused vertices fast
    std::set<long long int> edges;

};

std::map<long long int, vertex> load_graph(std::istream &is);

void store_graph(std::map<long long, vertex> graph, std::ostream &os);

void divide_random_edge(std::map<long long, vertex> &graph);


std::map<long long int, long long int> distances(std::map<long long int, vertex> &graph, long long int vertex);

std::set<std::pair<long long int, long long int>>
monitored_edges(std::map<long long int, vertex> &graph, long long int vertex);

long long int dem(std::map<long long int, vertex> &graph, std::chrono::milliseconds timeout);

long long int dem(std::map<long long int, vertex> &graph, std::set<long long> vertices_to_consider,
                  std::chrono::milliseconds timeout);

long long int
set_cover_dem(std::vector<std::pair<long long, std::set<std::pair<long long int, long long int>>>>::iterator begin,
              std::vector<std::pair<long long, std::set<std::pair<long long int, long long int>>>>::iterator end,
              std::set<std::pair<long long int, long long int>> edges_monitored, long long int num_edges,
              std::chrono::milliseconds timeout);


std::set<long long> fes_based_selection(std::map<long long int, vertex> graph);

void rr_leaf_removal(std::map<long long int, vertex> &graph);

void rr_independent_set(std::map<long long int, vertex> &graph);

std::map<long long int, vertex> generate_graph(long long int vc_size, long long int vertices, double edge_probability);

#endif //DEM_VC_GRAPH_H
