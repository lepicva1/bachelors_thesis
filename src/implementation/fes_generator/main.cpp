#include <iostream>
#include <vector>
#include <cstdlib>
#include <map>
#include <numeric>
#include <algorithm>
#include <map>
#include <utility>
#include <set>
#include "graph.h"


using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::map;


int main() {
    long long n,v,timeout;
        double ep;
    cout <<"starting_vertices edge_probability edge_divisions timeout[ms]"<<endl;
        cin>>v>>ep>>n>>timeout;
        auto graph = generate_graph(v,v,ep);
        for(int i = 0; i < n; ++i){
            divide_random_edge(graph);
        }
        cout << timeout<<endl;
        store_graph(graph,cout);
}
