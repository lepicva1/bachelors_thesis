//
// Created by Václav Lepič on 29.03.2022.
//

#include "graph.h"
#include <queue>
#include <limits>


using std::vector;
using std::map;

map<long long int, long long int> distances(map<long long int, vertex> &graph, long long int origin) {
    map<long long int, long long int> result;

    std::queue<std::pair<long long int, long long int>> q;
    q.push(std::make_pair(0, origin));
    std::set<long long int> visited;
    while (!q.empty()) {
        long long int distance = q.front().first;
        long long int current = q.front().second;
        q.pop();
        if (current == -1 || visited.find(current) != visited.end()) {
            continue;
        }
        visited.emplace(current);
        for (auto &edge: graph.find(current)->second.edges) {
            q.push(std::make_pair(distance + 1, edge));
            result.emplace(current, distance);
        }
    }
    return result;
}

void store_graph(std::map<long long, vertex> graph, std::ostream &os) {
    os << graph.size() << std::endl;
    for (auto &v: graph) {
        os << v.first << "\t";
        for (long long edge: v.second.edges) {
            os << " " << edge;
        }
        os << std::endl;
    }
}

std::map<long long int, vertex> load_graph(std::istream &is) {
    std::map<long long int, vertex> graph;
    long long size;
    is >> size;
    for (long long i = 0; i < size; ++i) {
        std::string line;
        std::getline(is, line, '\n');
        std::stringstream ss(line);
        vertex v;
        ss >> v.id;
        long long edge;
        while (ss >> edge) {
            v.edges.insert(edge);
        }
        graph.emplace(v.id, v);

    }
    return graph;
}

void rr_independent_set(std::map<long long int, vertex> &graph) {
    std::map<std::set<long long int>, long long int> map;
    long long size = graph.size();
    for (auto vertex_it = graph.begin(); vertex_it != graph.end(); ++vertex_it) {
        auto it = map.find(vertex_it->second.edges);
        if (it == map.end()) {
            auto edges = vertex_it->second.edges;
            map.emplace(move(edges), 1);
        }
        it = map.find(vertex_it->second.edges);

        //were there more vertices with the same neigborhood, than the size of this neighborhood?
        if (it->second > it->first.size()) {
            //remove edges to this vertex
            for (long long int vertex: it->first) {
                auto &edges = graph.find(vertex)->second.edges;
                edges.erase(edges.find(vertex_it->first));
            }
            //remove this vertex
            vertex_it = graph.erase(vertex_it);
            vertex_it--;
        } else {
            it->second += 1;
        }
    }
}


map<long long int, vertex> generate_graph(long long int vc_size, long long int vertices, double edge_probability) {
    map<long long int, vertex> graph;
    for (long long j = 0; j < vertices; ++j) {
        vertex v;
        v.id = j;
        if (j < vc_size) {
            graph.emplace(j, v);
            continue;
        }
        for (long long int i = 0; i < vc_size; ++i)
            if (rand() / (double) RAND_MAX < edge_probability) {
                v.edges.emplace(i);
                auto it = graph.find(i);
                it->second.edges.emplace(j);
            }
        graph.emplace(j, v);
    }
    for (int j = 0; j < vc_size; ++j) {
        vertex &v = graph.find(j)->second;
        for (long long int i = 0; i < j; ++i) {
            if (i == j)
                continue;
            if (rand() / (double) RAND_MAX < edge_probability) {
                v.edges.emplace(i);
                auto it = graph.find(i);
                it->second.edges.emplace(j);
            }
        }
    }
    return graph;
}

//taking graph as a copy is fully intentional
std::set<long long> fes_based_selection(std::map<long long int, vertex> graph) {
    for (auto it = graph.begin(); it != graph.end();) {
        vertex &v = it->second;
        if (v.edges.size() == 2) {
            vertex &first_neighbor = graph.find(*(v.edges.begin()))->second;
            vertex &second_neighbor = graph.find(*std::next(v.edges.begin()))->second;
            bool neighbors_adjacent = second_neighbor.edges.find(first_neighbor.id) != second_neighbor.edges.end();
            if (!neighbors_adjacent && (first_neighbor.edges.size() == 2 || second_neighbor.edges.size() == 2)) {
                first_neighbor.edges.emplace(second_neighbor.id);
                second_neighbor.edges.emplace(first_neighbor.id);
                first_neighbor.edges.erase(v.id);
                second_neighbor.edges.erase(v.id);
                it = graph.erase(it);
            } else
                ++it;
        } else {
            ++it;
        }
    }
    std::set<long long> result;
    for (auto &v: graph) {
        result.emplace(v.first);
    }
    return result;
}


std::set<std::pair<long long int, long long int>>
monitored_edges(map<long long int, vertex> &graph, long long int vertex) {
    std::set<std::pair<long long int, long long int>> result;
    for (auto &v: graph) {

        for (long long int edge: std::set<long long>(v.second.edges)) {
            auto orig_dist = distances(graph, vertex);
            v.second.edges.erase(edge);
            graph.find(edge)->second.edges.erase(v.first);
            auto new_dist = distances(graph, vertex);
            v.second.edges.emplace(edge);
            graph.find(edge)->second.edges.emplace(v.first);
            if (orig_dist != new_dist) {
                if (v.second.id < edge) {
                    result.emplace(v.second.id, edge);
                } else {
                    result.emplace(edge, v.second.id);
                }
            }
        }
    }
    return result;
}


long long int
set_cover_dem(std::vector<std::pair<long long, std::set<std::pair<long long int, long long int>>>>::iterator begin,
              std::vector<std::pair<long long, std::set<std::pair<long long int, long long int>>>>::iterator end,
              std::set<std::pair<long long int, long long int>> edges_monitored, long long int num_edges,
              std::chrono::milliseconds timeout) {
    auto start_time = std::chrono::system_clock::now();
    if (edges_monitored.size() == num_edges)
        return 0;
    if (begin == end)
        return INT_MAX;

    auto new_monitored = edges_monitored;
    for (std::pair<long long int, long long int> edge: begin->second)
        new_monitored.emplace(move(edge));
    //subtract time elapsed from timeout
    if (new_monitored.size() == edges_monitored.size()) {
        timeout -= std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start_time);
        if (timeout <= std::chrono::milliseconds(0)) {
            return -1;
        }
        return set_cover_dem(begin + 1, end, edges_monitored, num_edges, timeout);
    } else {
        auto new_timeout = timeout - std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now() - start_time);
        if (timeout <= std::chrono::milliseconds(0)) {
            return -1;
        }
        long long int not_added = set_cover_dem(begin + 1, end, edges_monitored, num_edges, new_timeout);
        timeout -= std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start_time);
        if (timeout <= std::chrono::milliseconds(0)) {
            return -1;
        }
        long long int added = set_cover_dem(begin + 1, end, new_monitored, num_edges, timeout) + 1;
        return std::min(added, not_added);
    }
}


long long int dem(std::map<long long int, vertex> &graph, std::set<long long> vertices_to_consider,
                  std::chrono::milliseconds timeout) {
    //count the total number of edges
    int num_edges = std::accumulate(graph.begin(), graph.end(), 0,
                                    [](long long int sum, auto&v) { return sum + v.second.edges.size(); }) / 2;
    //vertex id -> set of monitored edges
    std::vector<std::pair<long long, std::set<std::pair<long long, long long>>>> monitored;
    for (auto &vertex: graph) {
        if (vertices_to_consider.find(vertex.first) != vertices_to_consider.end())
            monitored.emplace_back(vertex.first, monitored_edges(graph, vertex.first));
    }

    return set_cover_dem(monitored.begin(), monitored.end(), std::set<std::pair<long long int, long long int>>(),
                         num_edges, timeout);
}

long long int dem(std::map<long long int, vertex> &graph, std::chrono::milliseconds timeout) {
    //count the total number of edges
    int num_edges = std::accumulate(graph.begin(), graph.end(), 0,
                                    [](long long int sum, auto&v) { return sum + v.second.edges.size(); }) / 2;
    //vertex id -> set of monitored edges
    std::vector<std::pair<long long, std::set<std::pair<long long, long long>>>> monitored;
    for (auto &vertex: graph) {
        monitored.emplace_back(vertex.first, monitored_edges(graph, vertex.first));
    }

    return set_cover_dem(monitored.begin(), monitored.end(), std::set<std::pair<long long int, long long int>>(),
                         num_edges, timeout);
}

void rr_leaf_removal(std::map<long long int, vertex> &graph) {
    for (auto it = graph.begin(); it != graph.end();) {

        if (it->second.edges.size() == 1) {
            long long neighbor = *it->second.edges.begin();
            if (graph.find(neighbor)->second.edges.size() > 1) {
                graph.find(neighbor)->second.edges.erase(it->first);
                it = graph.erase(it);
                continue;
            }
        }
        ++it;
    }
}

void divide_random_edge(std::map<long long, vertex> &graph) {
    auto item = graph.begin();
    std::advance(item, rand() % (graph.size() - 1));
    auto edge_it = item->second.edges.begin();
    while (item->second.edges.size() < 1) {
        item = graph.begin();
        std::advance(item, rand() % (graph.size() - 1));
        edge_it = item->second.edges.begin();
    }

    if (item->second.edges.size() > 1)
        std::advance(edge_it, rand() % (item->second.edges.size() - 1));
    vertex new_vertex;

    auto it = graph.begin();
    std::advance(it, graph.size() - 1);

    new_vertex.id = it->first + 1;
    new_vertex.edges.emplace(item->first);
    new_vertex.edges.emplace(*edge_it);
    graph.find(*edge_it)->second.edges.emplace(new_vertex.id);
    item->second.edges.emplace(new_vertex.id);

    graph.emplace(new_vertex.id, new_vertex);


    item->second.edges.erase(edge_it);
    graph.find(*edge_it)->second.edges.erase(item->first);
}
