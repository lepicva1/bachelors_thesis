#include <iostream>
#include <vector>
#include <cstdlib>
#include <map>
#include <numeric>
#include <algorithm>
#include <map>
#include <utility>
#include <set>
#include "graph.h"


using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::map;


int main() {
	long long n,vc, timeout;
	double ep;
    cout <<"vc_number vertex_count edge_probability timeout[ms]"<<endl;
	cin>>vc>>n>>ep>>timeout;
	auto graph = generate_graph(vc,n,ep);
    cout << timeout<<endl;
	store_graph(graph,cout);
}
