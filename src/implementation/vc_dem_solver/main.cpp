#include <iostream>
#include <vector>
#include <cstdlib>
#include <map>
#include <numeric>
#include <algorithm>
#include <map>
#include <utility>
#include <set>
#include "graph.h"


using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::map;


int main() {
    long long timeout;
    cin>>timeout;
    auto graph = load_graph(cin);
    rr_leaf_removal(graph);
    rr_independent_set(graph);
    cout << dem(graph, std::chrono::milliseconds (timeout))<<endl;
}
